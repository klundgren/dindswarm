#!/bin/bash -ex

BUILD_TAG=registry.gitlab.com/klundgren/$(basename $(PWD))

# Start a docker swarm in a docker container... (ignore warning and errors)
docker run --rm -ti -p "8080:80" -p "2375:2375" --privileged --name swarm ${BUILD_TAG}

# Portainer web config available at port 8080.
# (preferably, use incognito mode in web browser while testing)
# In Portainer, to scale docker swarm cluster (manager and worker nodes):
#   -> Services -> swarm-swarm-manager [...] Scale -> <enter number and hit enter>.
#   -> Services -> swarm-swarm-worker [...] Scale -> <enter number and hit enter>.
# In a shell, connect docker commandline to one of the docker swarm manager nodes:
#   > export DOCKER_HOST=<docker host>:2375
#   > docker node ls
