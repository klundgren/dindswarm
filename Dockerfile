FROM docker:stable-dind

RUN apk add --no-cache bash
RUN apk add --no-cache coreutils # sort --version-sort
RUN apk add --no-cache procps # ps -eo pid,etimes

COPY docker-root/ /
RUN chmod a+x /*.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD []
