BUILD_TAG=registry.gitlab.com/klundgren/$(notdir $(CURDIR))

all: docker-image

docker-image:
	docker build --pull -t ${BUILD_TAG} .

push:
	docker push ${BUILD_TAG}
