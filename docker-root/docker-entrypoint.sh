#!/bin/sh

exec 2>&1 # Redirect STDERR to STDOUT

set -x # verbose - on

ARG1=$1
shift

# Use exec to automatically relay signals (SIGTERM etc.)
exec bash /docker/${ARG1:-primary}/run.sh "$@"
