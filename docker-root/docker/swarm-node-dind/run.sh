#!/bin/bash

dockerd-entrypoint.sh \
  --userland-proxy=false \
  --registry-mirror=http://docker-registry:5000 &

set +x # verbose - off
# Wait for dind to come online:
while ! docker info 2> /dev/null; do echo .; sleep 1; done
set -x # verbose - on

MANAGER_OR_WORKER=$1
shift

# Scan for existing nodes, twize.
for run in {1..2}
do
  # Second run? -pause. (Other nodes may be initiating...)
  if [ "${run}" = "2" ]; then
    sleep 5
  fi

  MANAGER_IP_LIST=$(getent ahostsv4 tasks.swarm-manager \
                  | awk '{print $1}' \
                  | sort --version-sort \
                  | uniq )
  MY_IP=$(hostname -i)
  LOWEST_MANAGER_IP=$(echo ${MANAGER_IP_LIST} | awk '{print $1}')
  MANAGER_IP_LIST_OTHERS=$(echo ${MANAGER_IP_LIST} | sed "s|${MY_IP}||")

  # Scan other nodes for already initiated swarm:
  echo "  Scan other manager nodes for existing docker swarm..."
  for ip in ${MANAGER_IP_LIST_OTHERS}
  do
    TOKEN=$(export DOCKER_HOST=$ip:2375; docker swarm join-token ${MANAGER_OR_WORKER} -q)
    if [ -n "${TOKEN}" ]; then
      # Swarm found. -Join it!
      echo "  Joining existing docker swarm!"
      if docker swarm join --advertise-addr ${MY_IP} --token ${TOKEN} $ip; then
        SWARM_JOINED=true
      fi
      break
    fi
  done
  echo "  Scan finished."

  if [ "${SWARM_JOINED}" = "true" ]; then
    break
  fi

  # Create new swarm?
  if [ "${MANAGER_OR_WORKER}" = "manager" ]; then
    echo "  No existing docker swarm joined..."
    if [ "${MY_IP}" = "${LOWEST_MANAGER_IP}" ]
    then
      echo "  We have lowest IP of assigned swarm managers, initiating docker swarm..."
      docker swarm init --advertise-addr ${MY_IP}
      break
    else
      echo "  No existing swarm found and not assigned swarm creator..."
    fi
  fi
done

# Part of docker swarm? - if not, exit with error.
if [ "$(docker info --format "{{.Swarm.NodeAddr}}")" = "" ]; then
  echo Swarm not joined nor initiated, exiting.
  exit -1
fi

set +x # verbose - off

# Wait for Docker daemon to complete...
while [ -e /proc/$(pidof docker-containerd) ]; do sleep 1; done
