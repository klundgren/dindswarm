#!/bin/sh -ex

# Start docker daemon in docker (docker in docker, a.k.a. dind):
# Configuration:
#   --userland-proxy=false: Faster port forwarding setup: https://github.com/moby/moby/issues/14856
#   --registry-mirror: Cache docker images in local docker registry.
dockerd-entrypoint.sh \
  --userland-proxy=false &
#  --registry-mirror=https://in-house-registry-mirror:5000 &

set +x # verbose off
# Wait for docker to come online:
while ! docker info 2> /dev/null; do echo .; sleep 1; done
set -x # verbose on

docker swarm init
docker stack deploy -c /docker-compose.yml swarm

set +x # verbose off
# Wait until containers are running:
while docker system info --format "ContainersRunning: {{.ContainersRunning}}" | grep ' 0$'
do
  sleep 10
done

# Wait until no containers are running:
while docker system info --format "{{.SystemTime}} ContainersRunning: {{.ContainersRunning}}" | grep -v ' 0$'
do
  # UPTIME = Seconds PID 1 has been alive:
  UPTIME=$(ps -o pid,etimes | grep '^[[:space:]]*1[[:space:]]' | awk '{print $2}')
  if [[ -n "$DIND_MAX_UPTIME_S" && $UPTIME -gt ${DIND_MAX_UPTIME_S:-0} ]]
  then
    echo "  -Times up! UPTIME($UPTIME) > DIND_MAX_UPTIME_S($DIND_MAX_UPTIME_S)", aborting.
    exit 0
  fi
  sleep 10
done

echo No containers running. Exiting.
