#!/bin/bash

# Docker in docker needs to run in priviledged mode, docker swarm services cannot.
# ( https://github.com/docker/swarmkit/issues/1030 )
# Therefore, this service starts a docker in docker sub-container with:
# - priviledged mode.
# - re-uses the network interface from *this* container.
# Finally we attach console and unix signalling (SIGTERM, SIGKILL etc).

set -x # verbose - on
set -e # exit on error - on

# Cleanup in case of involuntary exit:
function cleanup {
  set -x # verbose - on
  docker rm -f ${DIND_HASH}
}
trap cleanup EXIT

DOCKER_IMAGE=$1
shift
MANAGER_OR_WORKER=$1

# Start privileged docker daemon in docker (docker in docker, a.k.a. dind):
DIND_HASH=$(docker run --rm -d \
  --privileged \
  --name=dind-$MANAGER_OR_WORKER-$HOSTNAME \
  --network=container:$HOSTNAME \
  ${DOCKER_IMAGE} \
  swarm-node-dind \
  "$@")

set +e # exit on error - off

# Attach console and unix signals to sub docker container:
docker attach --sig-proxy ${DIND_HASH}
